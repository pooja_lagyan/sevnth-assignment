package com.example.lagyan.fbloginapp;

/**
 * Created by LAGYAN on 10/19/2015.
 */
public class FbProfile
{
    String firstName;
    String lastName;
    int imgResID;

    public FbProfile(String firstName, String lastName, int imgResID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.imgResID = imgResID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }
}
