package com.example.lagyan.fbloginapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

/**
 * Created by LAGYAN on 10/19/2015.
 */
public class ListAdapter extends BaseAdapter {
    Context context;
    LinkedList<FbProfile> list;
    int rowLayout;

    public ListAdapter(Context context, LinkedList<FbProfile> list, int rowLayout) {
        this.context = context;
        this.list = list;
        this.rowLayout = rowLayout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = View.inflate(context,rowLayout,null);
        TextView firstName = (TextView)convertView.findViewById(R.id.firstname);
        TextView lastName = (TextView)convertView.findViewById(R.id.lastname);
        ImageView image = (ImageView)convertView.findViewById(R.id.image);

        firstName.setText(list.get(position).getFirstName());
        lastName.setText(list.get(position).getLastName());
        image.setImageResource(list.get(position).getImgResID());

        return convertView;
    }
}
